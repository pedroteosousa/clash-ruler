#!/usr/bin/python
import numpy as np
from tkinter import *
from tkinter import filedialog, simpledialog, colorchooser
from PIL import Image, ImageTk
import argparse
import math
import subprocess
import tempfile

class Spell:
    def __init__(self, disc, color):
        self.disc = list.copy(disc)
        self.color = color
        self.ids = []

class ClashGrid:
    AXIS1 = [0.8, 0.6]
    AXIS2 = [0.8, -0.6]
    MAP_SIZE = 50

    def __init__(self, sensibility):
        self.reset()
        self.sensibility = sensibility

    def reset(self, event = None):
        self.origin = None
        self.tile_size = None
        self.transform = None

    def ready(self):
        return self.origin is not None and self.transform is not None

    def update_transform(self, tile_size = None):
        self.tile_size = tile_size or self.tile_size
        self.transform = np.array([ClashGrid.AXIS1, ClashGrid.AXIS2]).transpose() * self.tile_size

    def handle_click(self, event):
        if self.origin is None:
            self.origin = np.array([event.x, event.y])
        elif self.transform is None:
            self.update_transform()

    def handle_move(self, event):
        if self.origin is not None and self.transform is None:
            self.tile_size = np.linalg.norm(np.array([event.x, event.y]) - self.origin) * self.sensibility

    def move_grid(self, event = None):
        self.origin = None

    def resize_grid(self, event = None):
        self.transform = None

    def to_pixels(self, points):
        pixels = []
        for p in points:
            pl = self.transform.dot(np.array(p))
            pixels.append(pl[0] + self.origin[0])
            pixels.append(pl[1] + self.origin[1])
        return pixels

class ClashCanvas:
    def __init__(self, grid, canvas, image_path):
        self.canvas = canvas
        self.grid = grid
        self.image_path = image_path
        self.clipboard = None
        self.image = Image.open(self.image_path) if self.image_path else None
        self.size = (1, 1)
        self.spells = []

    def ready(self):
        return self.grid.ready()

    def handle_click(self, event):
        center = [event.x, event.y]
        self.draw_grid(center)

    def handle_move(self, event):
        center = [event.x, event.y]
        self.draw_grid(center)

    def handle_resize(self, event):
        self.size = (event.width, event.height)
        self.update_image()

    def update_image(self):
        if not self.image_path and not self.clipboard:
            return

        old_size = self.image.size if self.image else (1, 1)

        self.image = Image.open(self.image_path) if self.image_path else self.clipboard.copy()
        original_size = self.image.size
        self.image.thumbnail(self.size)

        new_size = self.image.size
        ratio = new_size[0] / old_size[0]
        if self.grid.origin is not None:
            self.grid.origin = self.grid.origin * ratio
        if self.grid.transform is not None:
            self.grid.update_transform(self.grid.tile_size * ratio)

        self.imagetk = ImageTk.PhotoImage(self.image)
        self.canvas.delete('image')
        self.canvas.create_image(0, 0, image=self.imagetk, anchor=NW, tags='image')

        self.draw_spells()

    def open_image(self, event = None):
        path = filedialog.askopenfilename()
        if len(path):
            self.image_path = path
            self.clipboard = None
            self.update_image()

    def open_clipboard(self, event = None):
        self.image_path = None
        file = tempfile.NamedTemporaryFile()
        subprocess.call(("xclip", "-selection", "clipboard", "-t", "image/png", "-o"), stdout=file)
        self.clipboard = Image.open(file.name)
        file.close()
        self.update_image()

    def draw_point(self, point, color):
        point = self.grid.to_pixels([point])
        return self.canvas.create_oval(point[0] - 2, point[1] - 2, point[0] + 2, point[1] + 2, \
                outline=color, \
                fill=color)

    def draw_polygon(self, points, color, tag):
        points = self.grid.to_pixels(points)
        return self.canvas.create_polygon(points, outline=color, fill='', width=2, tags=tag)

    def add_spell(self, disc, color):
        spell = Spell(disc, color)
        self.draw_spell(spell)
        self.spells.append(spell)

    def undraw_spell(self, spell):
        for obj_id in spell.ids:
            self.canvas.delete(obj_id)
        spell.ids = []

    def remove_last_spell(self, *args):
        if len(self.spells):
            spell = self.spells.pop()
            self.undraw_spell(spell)

    def draw_spell(self, spell):
        self.undraw_spell(spell)
        spell.ids.append(self.draw_point(spell.disc[0], spell.color))
        spell.ids.append(self.draw_circle(spell.disc, spell.color, 'spell'))

    def draw_spells(self):
        for spell in self.spells:
            self.draw_spell(spell)

    def draw_tile(self, start):
        x = start[0]
        y = start[1]
        points = [[x, y], [x + 1, y], [x + 1, y + 1], [x, y + 1]]
        self.draw_polygon(points, '#f11', 'tile')

    def draw_circle(self, disc, color = '#1f1', tag = 'circle'):
        center, radius = disc
        points = []
        for theta in np.linspace(0, 2 * math.pi, 100, endpoint=False):
            x = math.cos(theta) * radius + center[0]
            y = math.sin(theta) * radius + center[1]
            points.append([x, y])
        return self.draw_polygon(points, color, tag)

    def draw_grid(self, origin = None):
        self.canvas.delete('grid')

        if self.grid.ready():
            return

        self.canvas.delete('circle')
        self.canvas.delete('tile')

        origin = origin if self.grid.origin is None else self.grid.origin
        MAX_TILE_SIZE = 1000
        tile_size = MAX_TILE_SIZE if self.grid.tile_size is None else self.grid.tile_size

        def draw_line(mid, axis, distance):
            x1 = mid[0] - axis[0] * distance
            y1 = mid[1] - axis[1] * distance
            x2 = mid[0] + axis[0] * distance
            y2 = mid[1] + axis[1] * distance
            self.canvas.create_line(x1, y1, x2, y2, tags='grid')

        DIF = [ClashGrid.AXIS1[0] + ClashGrid.AXIS2[0], ClashGrid.AXIS1[1] + ClashGrid.AXIS2[1]]
        MAP_SIZE = ClashGrid.MAP_SIZE
        GRID_LINE_SIZE = 2 * tile_size * MAP_SIZE
        for i in range(-MAP_SIZE, MAP_SIZE):
            midx = origin[0] + i * DIF[0] * tile_size
            midy = origin[1] + i * DIF[1] * tile_size
            draw_line([midx, midy], ClashGrid.AXIS1, GRID_LINE_SIZE)
            draw_line([midx, midy], ClashGrid.AXIS2, GRID_LINE_SIZE)

SPELLS = {
    'Light': (2, '#09d'),
    'Earthquake 1': (3.5, '#853'),
    'Earthquake 2': (3.8, '#853'),
    'Earthquake 3': (4.1, '#853'),
    'Earthquake 4': (4.4, '#853'),
    'Earthquake 5': (4.7, '#853')
}

class ClashHandler:
    def __init__(self, grid, clash_canvas):
        self.grid = grid
        self.clash_canvas = clash_canvas
        self.disc = [None, 2.0]
        self.color = '#09d'

    # check if tile 'start' intersects self.disc
    def intersects(self, start):
        # check if point in inside the disc
        def inside(point, disc):
            center, radius = disc
            return np.linalg.norm(np.array(point) - np.array(center)) < radius

        # check if x in inside segment (v1, v2)
        def between(x, v1, v2):
            return x > min(v1, v2) and x < max(v1, v2)

        # check if segment intersects disc
        def intersects(segment, disc):
            start, end = segment
            center, radius = disc
            if inside(start, disc) or inside(end, disc):
                return True
            distance = None
            if start[0] == end[0]:
                if not between(center[1], start[1], end[1]):
                    return False
                return abs(center[0] - start[0]) < radius
            else:
                if not between(center[0], start[0], end[0]):
                    return False
                return abs(center[1] - start[1]) < radius

        x = start[0]
        y = start[1]
        points = [[x, y], [x + 1, y], [x + 1, y + 1], [x, y + 1]]
        center = self.disc[0]
        for i in range(4):
            segment = [points[i], points[(i + 1) % 4]]
            if intersects(segment, self.disc) or (between(center[0], x, x + 1) and between(center[1], y, y + 1)):
                return True
        return False

    def handle_click(self, event):
        self.grid.handle_click(event)
        self.clash_canvas.handle_click(event)

    def handle_move(self, event):
        self.grid.handle_move(event)
        self.clash_canvas.handle_move(event)

        if not self.grid.ready():
            return

        origin = self.grid.origin
        center = np.linalg.inv(self.grid.transform).dot(np.array([event.x - origin[0], event.y - origin[1]]))
        self.clash_canvas.canvas.delete('tile')
        self.clash_canvas.canvas.delete('circle')
        self.disc[0] = center
        self.clash_canvas.draw_circle(self.disc)
        # TODO: run a BFS instead of trying all tiles in a large square
        start = [int(center[0]), int(center[1])]
        offset = 2 * math.ceil(self.disc[1])
        for i in range(start[0] - offset, start[0] + offset):
            for j in range(start[1] - offset, start[1] + offset):
                if self.intersects([i, j]):
                    self.clash_canvas.draw_tile([i, j])

    def save_spell(self, *args):
        self.clash_canvas.add_spell(self.disc, self.color)

    def set_radius(self, radius):
        self.disc[1] = radius

    def choose_radius(self):
        self.disc[1] = simpledialog.askfloat('Enter new spell radius', 'radius', \
                initialvalue=self.disc[1], maxvalue=10.0, minvalue=0.0) or self.disc[1]

    def choose_color(self):
        self.color = colorchooser.askcolor(color=self.color)[1] or self.color

    def set_spell(self, spell_name):
        if spell_name in SPELLS:
            radius, color = SPELLS[spell_name]
            self.disc[1] = radius
            self.color = color

class App(Tk):
    def __init__(self):
        parser = argparse.ArgumentParser(description='Practice with clash of clans spells')
        parser.add_argument('image_path', type=str, nargs='?', help='path to a screenshot of a base')
        parser.add_argument('--sensibility', default=0.1, type=float, help='sensibility of grid setup')
        args = parser.parse_args()

        Tk.__init__(self)
        canvas = Canvas(self)
        canvas.pack(expand=True, fill='both')

        grid = ClashGrid(args.sensibility)
        cc = ClashCanvas(grid, canvas, args.image_path)
        ch = ClashHandler(grid, cc)

        menu = Menu(self)
        open_menu = Menu(menu, tearoff=False)
        open_menu.add_command(label="Image", command=cc.open_image, accelerator="Ctrl+O")
        open_menu.add_command(label="Clipboard", command=cc.open_clipboard, accelerator="Ctrl+V")
        menu.add_cascade(label="Open", menu=open_menu)
        spell_menu = Menu(menu, tearoff=False)
        spell_menu.add_command(label="Light", command=lambda: ch.set_spell('Light'), accelerator="L")
        spell_menu.add_command(label="Earthquake 1", command=lambda: ch.set_spell('Earthquake 1'), accelerator="1")
        spell_menu.add_command(label="Earthquake 2", command=lambda: ch.set_spell('Earthquake 2'), accelerator="2")
        spell_menu.add_command(label="Earthquake 3", command=lambda: ch.set_spell('Earthquake 3'), accelerator="3")
        spell_menu.add_command(label="Earthquake 4", command=lambda: ch.set_spell('Earthquake 4'), accelerator="4")
        spell_menu.add_command(label="Earthquake 5", command=lambda: ch.set_spell('Earthquake 5'), accelerator="5")
        menu.add_cascade(label="Spells", menu=spell_menu)
        grid_menu = Menu(menu, tearoff=False)
        grid_menu.add_command(label="Move", command=grid.move_grid, accelerator="Ctrl+M")
        grid_menu.add_command(label="Resize", command=grid.resize_grid, accelerator="Ctrl+R")
        grid_menu.add_command(label="Reset", command=grid.reset)
        menu.add_cascade(label="Grid", menu=grid_menu)
        menu.add_command(label="Color", command=ch.choose_color)
        self.config(menu=menu)

        self.bind('<Control-m>', grid.move_grid)
        self.bind('<Control-r>', grid.resize_grid)
        self.bind('<Control-s>', ch.save_spell)
        self.bind('<Control-z>', cc.remove_last_spell)
        self.bind('<Configure>', cc.handle_resize)
        self.bind('<Control-o>', cc.open_image)
        self.bind('<Control-v>', cc.open_clipboard)
        canvas.bind('<Button-1>', ch.handle_click)
        canvas.bind('<Motion>', ch.handle_move)

        # spells
        self.bind('l', lambda _: ch.set_spell('Light'))
        self.bind('L', lambda _: ch.set_spell('Light'))
        self.bind('1', lambda _: ch.set_spell('Earthquake 1'))
        self.bind('2', lambda _: ch.set_spell('Earthquake 2'))
        self.bind('3', lambda _: ch.set_spell('Earthquake 3'))
        self.bind('4', lambda _: ch.set_spell('Earthquake 4'))
        self.bind('5', lambda _: ch.set_spell('Earthquake 5'))

if __name__ == '__main__':
    app = App()
    app.mainloop()
