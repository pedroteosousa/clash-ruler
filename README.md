# Clash Ruler (a.k.a Raimunda)

This is a program to help you hit your targets when using spells on [Clash of Clans](https://supercell.com/en/games/clashofclans/). You just need to open / paste an image and set up the grid.

## Web Version

There is a version of this project running on heroku, here is the repository: https://gitlab.com/pedroteosousa/clash-ruler-web

They are not the same program, some features you will only find here, some only there, use the best one for your needs.

## Usage

Just run it with

`python clash-ruler.py`

Just click on the edge of a tile and drag the mouse so the grid shown on screen matches the game grid.

![Usage example](assets/example.gif)

You can use the `--sensibility` flag to define how fast the grid expands on setup.

There is also a Makefile to install the script on `~/.local/bin`, if that is more convenient, then you can run it with `clash-ruler`.
